module.exports = {
    baseUrl: './',
    outputDir: 'dist',
    assetsDir: 'static',
    runtimeCompiler: false,
    productionSourceMap: false,
    lintOnSave: false,
    devServer: {
        proxy: {
            '/api': {
                target: 'http://sale.51cubi.com',
                pathRewrite: { '^/api': '/' },
                changeOrigin: true
            },
            '/foo': {
                target: '<other_url>'
            }
        }
    }
}
