import post from './post'

export const getHomeInfo = param => {
    return post('/Home/Banners', param)
}
export const getArticleList = param => {
    return post('/Home/ArticleList', param)
}
export const getUserInfo = param => {
    return post('/Sale/GetUser', param)
}
export const getMyTeam = param => {
    return post('/Sale/MyTeam', param)
}
export const getMyTeamRecord = param => {
    return post('/Sale/MyTeamRecord', param)
}
export const getInfoForAddOrder = param => {
    return post('/Sale/GetInfoForAddOrder', param)
}
export const addOrder = param => {
    return post('/Sale/AddOrder', param)
}
export const getDynamictoTotalInfo = param => {
    return post('/Sale/GetDynamictoTotalInfo', param)
}
export const doDynamicToTotal = param => {
    return post('/Sale/DynamicToTotal', param)
}
export const getMyFinanceRecord = param => {
    return post('/Sale/MyFinanceRecord', param)
}
export const getProductList = param => {
    return post('/Mall/ProductList', param)
}
export const getProduct = param => {
    return post('/Mall/Product', param)
}
export const createdOrder = param => {
    return post('/Mall/AddOrder', param)
}
export const getWithdrawInfo = param => {
    return post('/Sale/GetWithdrawInfo', param)
}
export const doWithdraw = param => {
    return post('/Sale/Withdraw', param)
}
export const InfoForExchange = param => {
    return post('/Exchange/InfoForExchange', param)
}
export const doDeal = param => {
    return post('/Exchange/Deal', param)
}
export const cancelDeal = param => {
    return post('/Exchange/CancelDeal', param)
}
export const getArticleDet = param => {
    return post('/Home/Article', param)
}