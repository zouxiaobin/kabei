import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Mint from 'mint-ui'

// 样式初始化以及解决border的1px的问题
import './assets/css/reset.css'
import './assets/css/border.css'
// 使用mintUI
import 'mint-ui/lib/style.min.css'
// 适配
import 'amfe-flexible'
// 全局过滤器
import * as filters from './filters'

// 中间件使用
Vue.use(Mint)

// api
import * as Api from '@/api/api'

// 在原型挂载对应的的公共方法
Vue.prototype.$post = Api // API

// 注册全局过滤器
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
