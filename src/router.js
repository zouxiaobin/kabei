import Vue from 'vue'
import Router from 'vue-router'
import index from './views/index'

Vue.use(Router)

export default new Router({
//  mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: index
        },
        {
            path: '/profit',
            name: 'profit',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/profit')
        },
        {
            path: '/generalize',
            name: 'generalize',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/generalize')
        },
        {
            path: '/runningwater',
            name: 'runningwater',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/runningwater')
        },
        {
            path: '/team',
            name: 'team',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/team')
        },
        {
            path: '/teamrecord',
            name: 'teamrecord',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/teamrecord')
        },
        {
            path: '/exchange',
            name: 'exchange',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/exchange')
        },
        {
            path: '/shop',
            name: 'shop',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/shop')
        },
        {
            path: '/my',
            name: 'my',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/my')
        },
        {
            path: '/declaration',
            name: 'declaration',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/declaration')
        },
        {
            path: '/qccash',
            name: 'qccash',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/qccash')
        },
        {
            path: '/c2ccashe',
            name: 'c2ccashe',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/c2ccashe')
        },
        {
            path: '/articledet',
            name: 'articledet',
            component: () =>
                import(/* webpackChunkName: "about" */ './views/articledet')
        }
    ]
})
